<?php

namespace Drupal\multi_form\Controller;

use Drupal\Core\Controller\ControllerBase;

class MultiStepController extends ControllerBase
{
  public function multiFormPage() {
    $form = \Drupal::formBuilder()->getForm('Drupal\multi_form\Form\MultiStepFormFirst');
    return $form;
  }
}
